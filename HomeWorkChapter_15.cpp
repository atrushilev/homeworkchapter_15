#include <iostream>
using namespace std;

void OddEven(bool odd, int limit)
{
    for(int i = odd; i <= limit; i = i + 2)
    {
        //i = i + 1;
        cout << i << " ";
    }
}

int main()
{
    int N = 10;
    cout << "Even numbers are: ";
    for (int i = 1; i <= N; i++)
    {
        if (i % 2 == 0)
            cout << i << " ";
    }
    cout << "\n";
    
    OddEven(false, 30);
}

